# Badge Application Requirements

## Compliance Verification Procedures Requirements

AAP compliance verification procedures leverage the tests, compliance verification tools,
and test infrastructure defined and maintained by LF Networking projects which are
included in an open source release of one of the related Series LLC. The Series LLC TSC
defines which compliance verification procedures are included as part of the AAP. Once
published, the compliance verification procedure suites will not change until the next
AAP release (except to fix bugs or errors), as described in the
[Program Management & Maintenance](program_management_and_maintenance.md).

AAP compliance verification is applicable to one or more of the following categories:

1. Hardware Platform
1. Software Platform (e.g, Virtual and Cloud Native Infrastructure – NFVI, VIM, etc.)
1. Applications (e.g. VNFs or CNFs)  
1. Orchestration (End to End management)

The scope of the criteria and requirements for each AAP release is set forth in an
addendum to this document, available as part of each Anuket Assured Program release.

## Self-Compliance Verification

Organizations may conduct compliance verification using the LF Networking-designated
tools at their facilities. The LF Networking-designated tools will ensure that the
results were produced by an unaltered version of the tools, and that the results
were unaltered.  For example, this could be accomplished by digitally verifying the
tools themselves, and signing the results file(s).  Results MUST be submitted for
review with application documentation and the logo usage agreement to LF Networking.

## Qualified Verification Labs

Vendors may request service from third parties to conduct LF Networking compliance
verification and collect results. Compliance verification occurs as documented in
“Self Compliance Verification” above.  The compliance verification results and
documentation may be submitted by the third party on behalf of the vendor requesting
the use of LF Networking Program Marks.

LF Networking may identify organizations providing third-party verification as
Qualified Labs and list them on the LFN web site. LF Networking does not endorse or
sponsor Qualified Labs’ services. Vendors are not required to use LF Networking
Qualified Labs.

## Compliance Application Requirements

The use of the program logo is not restricted to LF Networking member companies.
The request for use of LF Networking program marks must state the organization, a
contact person (email and telephone number); their postal address; the location
where the verification results and documentation was prepared, the category or
categories the product is requesting a program marks(s) for; the attestation
stating they will abide by the policies and procedures for use of the program
marks; any third-party lab conducting the verification; and the product identifier.

## Review Process

The compliance verification results and documentation submitted will be reviewed
by the Committee for completeness and validity.  Based on the determination of the
Committee, a recommendation will be made to the LFN Board of Directors regarding
the approval of the granting of permission to use the program marks.

The Committee may request additional information regarding the application for
use of the program marks.

The LFN may charge a reasonable fee for reviewing results.  Reviews will be
conducted by LFN member companies participating in the review committee
(a C&V subcommittee). No member company may review its own compliance verification
results.

In the event of a dispute, the submitting organization has a right to appeal the
decision with the LFN Board of Directors. An appeals process is documented in the
[Escalation Process](escalation_process.md).
