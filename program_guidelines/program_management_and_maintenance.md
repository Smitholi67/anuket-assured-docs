# Program Management & Maintenance

## Role of C&V Committee

The Compliance & Verification (C&V) Committee, hereafter referred to as the
Committee, serves as the AAP administrator on behalf of the Linux Foundation
Networking (LFN) Board of Directors.  The Committee is responsible for
defining program governance, compliance verification strategy, and the overall
scope of the compliance verification procedures.

## Maintenance of Program Documents

Program documents, such as this document, produced by the Committee will be
labeled using semantic versioning.  That is, they will be labeled using
MAJOR.MINOR.PATCH notation, where MAJOR, MINOR, and PATCH are non-negative
integers.

1. MAJOR version. Note: to avoid market confusion, the scope of compliance
verification and other project documents is tied to the major version, and
once approved by the Board of Directors, will not change until the next
major release.
1. MINOR version is used to denote significant functionality changes
(e.g., addition/subtraction of compliance verification procedures and/or
document sections).  Changes to a minor version of an approved document
require Board approval. It is a goal that minor changes should not affect
the ability of a product to achieve compliance/qualification status.
1. PATCH version is used to indicate error corrections or editorial changes.

The scope of a particular version of the requirements, test-cases, compliance
verification checks, tools and results is tied to an AAP release.  

## Maintenance of AAP compliance verification procedures

The overall AAP scope is determined by the output of the LFN projects,
and will be updated on a regular cadence (e.g., approximately twice per year).
The LFN Series LLC TSC(s) defines and maintains the compliance verification
procedures and associated tools. The scope is constrained to features, capabilities,
components, and interfaces included in an Anuket, ONAP, or other LFN project
releases that are generally available in the industry (e.g., through adoption by
an upstream community). The scope also includes software designed to work with
such projects and supporting requirements defined in the project(s), e.g., VNFs or
CNFs/ Compliance verification is evaluated using functional tests that focus on
defined interfaces and/or behaviors without regard to the underlying system
under test. In practice, testing exercises interfaces and/or behaviors developed
or exposed by Anuket, ONAP, etc.

## LFN Program Marks Usage Guidelines

Because of its position in the industry as an Open Source Project maintained
by the Linux Foundation, LFN cannot endorse any products or appear to endorse
any products.

## Use of the Term "Anuket Assured” and/or related program marks

Products that pass compliance verification suite(s) may be labeled as
“Anuket Assured”.  Products that are “Anuket Assured” expose key features/behaviors
of the AAP release.

“Anuket Assured” asserts that a specific system under test:

* Supply the key behaviors, functions, and related APIs
* Promotes the key values of the release, e.g. based upon the open source
project components that comprise the release
  * Performs basic NFV and cloud native functions
  * Is interoperable with the applicable open source ecosystem
  * Suitable for further trials in an operator network
* “Anuket Assured” does not assert:
  * Readiness for commercial deployment

The C&V Committee on behalf of the Board of Directors can award a product
"Anuket Assured" or related status. “Anuket Assured”, therefore, may not
be used in relation to a vendor’s product without first having met the
requirements outlined in this document and the LF Networking Terms and
Conditions.

Use of “Anuket Assured” is determined by the Terms and Conditions and the
Branding Guide. The programs standards and requirements will evolve over time
as the platforms and projects mature.

Organizations applying for compliance verification shall use the program marks
solely for the qualified offering that was verified to have met the requirements
designated by The Committee with respect to the appropriate category.

They shall not use the program marks for any product/technology other than the
Product/Technology submitted for compliance verification even if it belongs to
the appropriate category.

They shall only use the program marks solely for the following purposes in order to:

* Promote LF Networking series of projects;
* Indicate to procurers and users the information of interoperability for NFV and
cloud native infrastructure and applications (e.g. VNFs and CNFs); and
* indicate that the verified product meets all requirements set by The Committee
for use of the Program Marks.

Organizations shall not use the program marks in any way that would associate it
with any individual or company logo or brand, beyond the association to the
specific platform to which it was awarded.

They shall use the program marks solely in accordance with the Branding Guide
which is prepared and amended by the LFN from time to time. Other than in
association to the specific platform to which it was awarded, they shall not frame,
post, upload, transmit or modify in any way, all or any part of the Program Marks,
unless expressly authorized in writing by the Committee.

Organizations shall immediately notify the Committee if they suspect or discover
that the Program Mark(s) is or will be used for any purposes other than the
confirmed purposes or that the use conflicts with any of the representations hereof
as a result of upgrading of its submitted product/technology. In the event that the
above notification is provided, they shall provide the Committee with any and all
information requested by the Committee in relation to the upgrading of the confirmed
product/technology and all information in order to confirm the revised product/technology
actually meets the requirements designated by The Committee with respect to the appropriate
category. They shall not use the Program Marks for any product/technology which does not
meet the requirements designated by the Committee with respect to the confirmed category.

Note: Such organizations will be held responsible for any illegal use of the program
marks by others.

Organizations participating in the LFN AAP do not own any intellectual property rights
in relation to the Program Marks, program documents, or compliance verification procedures.
