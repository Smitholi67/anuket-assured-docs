# Purpose of this Document

This document defines the framework and governance of the LFN Anuket Assured
Program (AAP), including the scope and objectives of the program, maintenance
of program materials, compliance verification requirements and processes,
program mark usage guidelines, requirements for systems under compliance
verification, and escalation procedures.

This document does not define compliance verification test procedures. Compliance
verification test procedures are defined by the community under the oversight of
the Technical Steering Committees within the revelent project.

The current scope of compliance verification is based on multiple sources:

* The release(s) of the Anuket specifications
* The release(s) of the Anuket Reference Compliance test suites
* ONAP VNF requirements and test specifications
* Informative ETSI NFV ISG specifications, specifically: Pre-deployment Testing: [Report on Validation of NFV Environments and Services](http://www.etsi.org/deliver/etsi_gs/NFV-TST/001_099/001/01.01.01_60/gs_NFV-TST001v010101p.pdf)
* ONAP VNF Requirements

Please note that these sources are subject to further revisions and may be updated
at some future time. The current compliance verification procedures are published
by LF Networking.
