# Anuket Assured 2022.10 Release

## Introduction

This addendum provides a high-level description of the testing scope and
pass/fail criteria used in the Anuket Assured Program (AAP) for the above
release. This information is intended as an overview for AAP testers to help
guide test-tool and test-case development for the release. The individual
projects, such as Anuket, ONAP, or others, are responsible for documenting
test-case specifications, as well as implementing the tool-chains through
collaboration within the project communities. AAP testing focuses on
establishing the ability of the System Under Test (SUT) to perform NFVI, VIM,
Cloud Native Infrastructure, and CNF operations to support Service Provider
oriented features that ensure manageable, resilient and secure networks.

## Meaning of Conformance

AAP Conformance indicates adherence of an NFVI or Cloud Native platform and VNF/CNF
to behaviors defined through specific platform capabilities, allowing to prepare,
instantiate, operate and remove xNFs running on the NFVI/Cloud. The release
conformance evaluates the ability of a platform to support Service Provider network
capabilities and workloads that are supported in the Anuket and ONAP platforms
as of this release. Test cases are designated as compulsory or optional based on
the maturity of capabilities as well as industry expectations. Compulsory test
cases might, for example, include NFVI or CNF management capabilities whereas
tests for certain high-availability features may be deemed as optional.

Test coverage and pass/fail criteria are designed to ensure an acceptable level
of conformance but not be so restrictive as to disqualify variations in platform
implementations, capabilities and features.

## SUT Assumptions

### NFVI Infrastructure Badge

Assumptions about the NFVI System Under Test (SUT) for the AAP NFVI
Infrastructure badge include …

The minimal specification of physical infrastructure, including controller nodes,
compute nodes and networks, is defined for the NFVI by the Anuket RI specifications.

The SUT is fully deployed and operational, i.e. SUT deployment tools are out of
scope of testing.

### VNF Workload Badge

Assumptions about the VNF System Under Test (SUT) for the AAP VNF badge include …

The VNF templates and disk image(s) file are available, and the disk image(s)
have been uploaded to the ONAP Cloud Site.

The required value for the VNF pre-load files are available for the selected
ONAP Cloud Site.

### Cloud Native Infrastructure Badge

The System Under Test (SUT) should already be deployed and would be expected
to meet the requirements of the Anuket Reference Architecture release 2 (RA2),
as defined within the Kali release. For this deployment the minimum topology or
configuration is one controller node and two workers nodes.

### Cloud Native Workload Badge

Cloud Native Workload testing is out of scope for this release of the
Anuket Assured Program.

## Scope of Testing

The Governance Guidelines, as approved by the LFN Board of Directors, outlines
the key objectives of the programs as follows:

* Help enable the market for infrastructure and applications designed
to run on technologies aligned to LFN specifications
* Reduce risks for end-users adopting these technologies
* Decrease testing costs by verifying hardware and software platform interfaces
and components
* Enhance interoperability between platforms and applications

The guidelines further directs the scope to be constrained to “features,
capabilities, components, and interfaces included in Anuket and ONAP
releases that are generally available in the industry (e.g., through
adoption by an upstream community)”, and that conformance verification is
evaluated using “functional tests that focus on defined interfaces and/or
behaviors without regard to the implementation of the underlying system under
test”.

Anuket provides a broad range of specifications for virtual and cloud native
systems, including at least 1 reference platform(s) implementation of the
applicable specifications. As well as tools-chains and methodologies for building
infrastructures, and deploying and testing the platform. Not all these aspects are
in scope for this program and not all functions and components are tested in each
release of this program. For example, the deployment tools for the SUT and CI/CD
toolchain are currently out of scope. Similarly, performance benchmarking related testing
is also out of scope and a subject for further study. Newer functional areas such as
MANO (outside of APIs in the infrastructure or workload) are still developing
and are for future consideration.

ONAP provides a comprehensive platform for real-time, policy-driven orchestration
and automation of physical and virtual network functions that will enable
software, network, IT and cloud providers and developers to rapidly automate
new services and support complete lifecycle management. By unifying member
resources, ONAP is accelerating the development of a vibrant ecosystem around a
globally shared architecture and implementation for network automation – with an
open standards focus – faster than any one product could on its own.

## Analysis of Scope

In order to define the scope of this release, this
section analyzes NFV-focused platform capabilities with respect to the
high-level objectives and the general approach of the program. The analysis
determines which capabilities are suitable for inclusion in this release of
the program and which capabilities are to be addressed in future releases.

### NFVI Badges

The intent of the Anuet RC1 tests is to verify that the SUT has the required
capabilities that a basic VNF needs, and these capabilities are implemented
in a way that enables this basic VNF to run on any Anuket compliant deployment.

A basic VNF can be thought of as a single virtual machine that is networked and
can perform the simplest network functions, for example, a simple forwarding
gateway, or a set of such virtual machines connected only by simple virtual
network services. Running such basic VNF leads to a set of common requirements,
including:

* image management (testing Glance API)
* identity management (testing Keystone Identity API)
* virtual compute (testing Nova Compute API)
* virtual storage (testing Cinder API)
* virtual networks (testing Neutron Network API)
* forwarding packets through virtual networks in data path
* filtering packets based on security rules and port security in data path
* dynamic network runtime operations through the life of a VNF (e.g.
attach/detach, enable/disable, read stats)
* correct behavior after common virtual machine life cycles events (e.g. suspend/resume, reboot, migrate)
* simple virtual machine resource scheduling on multiple nodes

The AAP supports OpenStack as the VIM based on the Anuket Kali release. The VNFs
used in the NFVI badge program, and features in scope for the program which are
considered to be basic to all VNFs, require commercial OpenStack distributions
to support a common basic level of virtualization capabilities, and to be compliant to a
common specification for these capabilities. This requirement significantly
overlaps with OpenStack community’s Interop working group’s goals, but they are
not identical. The AAP runs some of the OpenStack Refstack-Compute test
cases to verify conformance to the basic common API requirements of
management functions and VNF (as a VM) management for Anuket. Additional NFV
specific requirements are added in network data path validation, packet filtering
by security group rules and port security, life cycle runtime events of virtual
networks, multiple networks in a topology, validation of VNF’s functional state
after common life-cycle events including reboot, pause, suspense, stop/start and
cold migration. In addition, the basic requirement also verifies that the SUT can
allocate VNF resources based on simple anti-affinity rules.

The combined test cases help to ensure that these basic operations are always
supported by a compliant platform and they adhere to a common standard to enable
portability across Anuket Assured platforms.

The program utilizes the same set of test cases as the OpenStack interoperability
program OpenStack Powered Compute. meeting the requirements of this program does
not imply that the SUT is certified according to the OpenStack Powered Compute
program. OpenStack Powered Compute is a trademark of the OpenStack foundation and
the corresponding certification label can only be awarded by the OpenStack
foundation.

#### Stress Testing

Resiliency testing involves stressing the SUT and verifying its ability to absorb
stress conditions and still provide an acceptable level of service. Resiliency is
an important requirement for end-users.

This release of AAP includes API load tests, which ensure that the controller software
can handle larger volumes of API requests without failures or missed responses.  At this
time, the AAP program does not include tests for loading or stressing the worker
or virtual resource hosting of the SUT.

#### Security

Security is among the top priorities as a carrier grade requirement by the
end-users. Some of the basic common functions, including virtual network
isolation, security groups, port security and role based access control
are already covered as part of the basic cloud capabilities. These requirements
are not yet captured within the requirements of the Anuket Reference Conformance
release 1 (RC1) requirements, and will be addressed within a future release of
this program. It is an area that we should address in the near future, to
define a common set of requirements and develop test cases for verifying
those requirements.

#### Service assurance

Service assurance (SA) is a broad area of concern for reliability of the
NFVI/VIM and VNFs, and depends upon multiple subsystems of an NFV platform
for essential information and control mechanisms. These subsystems include
telemetry, fault management (e.g. alarms), performance management, audits,
and control mechanisms such as security and configuration policies.

This release implements some enabling capabilities in NFVI/VIM such as
telemetry, policy, and fault management. However, the specification of
expected system components, behavior and the test cases to verify them
have not yet been adequately developed. We will therefore not be testing
this area at this time but defer for future study.

## VNF Workload Badge Components

VNF Compliance verifies the VNF template files conform to the requirements
documented in by ONAP VNFRQTS Basic Cloud Capabilities project.

VNF Validation verifies the VNF is able to onboard within ONAP and ONAP
is able to perform basic orchestration operations with the VNF, including
instantiating the VNF on the Cloud Site.

## Scope of testing for this release

Summarizing the results of the analysis above, the scope of this release of
Anuket Assured Program is as follows:

### Mandatory NFV Infrastructure test scope

The required conformance test cases for this release are defined as the
mandatory set of test cases within the Anuket Kali Release of the Reference
Conformance (RC1).  RC1 Chapter 3 describes the full set of required test
cases, as well as their applicable references to the Anuket Reference Model
(RM1) and Anuket Reference Architecture (RA1).

* [Anuket Rererence Conformance RC1 (read the docs)](https://cntt.readthedocs.io/en/stable-kali/ref_cert/RC1/)
* [Anuket Reference Conformance RC1 (Github)](https://github.com/cntt-n/CNTT/tree/stable/kali/doc/ref_cert/RC1)

### Mandatory VNF Workload test scope

Refer to ONAP VNF Test Case Descriptions as defined in the ONAP VNFREQS for the
Honolulu release.  Testing is implemented through the VNFSDK and VVP components
of the ONAP release.

* [ONAP VNF and PNF Requirements and Guidelines](https://docs.onap.org/en/honolulu/guides/onap-provider/index.html)
* [ONAP VNF Requirements](https://docs.onap.org/projects/onap-vnfrqts-requirements/en/honolulu/Chapter1/index.html)
* [ONAP VNF Test Cases](https://docs.onap.org/projects/onap-vnfrqts-testcases/en/honolulu/index.html#master-index)

### Mandatory Cloud Native Infrastructure test scope

The required conformance test cases for this release are defined as the
mandatory set of test cases within the Anuket Kali Release of the Reference
Conformance (RC2).  RC2 Chapter 2 describes the full set of required test
cases, as well as their applicable references to the Anuket Reference Model
(RM2) and Anuket Reference Architecture (RA2).

* [Anuket Rererence Conformance RC2 (read the docs)](https://cntt.readthedocs.io/en/stable-kali/ref_cert/RC2/)
* [Anuket Reference Conformance RC2 (Github)](https://github.com/cntt-n/CNTT/tree/stable/kali/doc/ref_cert/RC2)

## Mandatory CNF Workload test scope

No CNF Workload badges are available in this release of the program, but are
anticipated for a subsequent release of the program.

## Scope considerations for future releases

Based on the previous analysis, the following items are outside the scope of
this release of the program but are being considered for inclusion in future
releases:

* service assurance
* use case testing
* platform in-place upgrade
* API backward compatibility / micro-versioning
* workload migration
* multi-site federation
* service function chaining
* platform operational insights, e.g. telemetry, logging efficiency,
e.g. hardware and energy footprint of the platform
* resilience
* security and vulnerability scanning
* performance or benchmark measurements

## Criteria for Awarding Conformance

This section provides guidance on conformance criteria for each test area. The
criteria described here are high-level, detailed pass/fail metrics are
documented in test specifications referenced above.

All mandatory test cases must pass.

Exceptions to this rule may be legitimate, e.g. due to imperfect test tools or
reasonable circumstances that we can not foresee. These exceptions must be
documented and accepted by the reviewers, through the approved exception process.

Optional test cases are optional to run. Its test results, pass or fail, do not
impact conformance or award of the badge.

Applicants who choose to run the optional test cases can include the results of
the optional test cases to highlight the additional conformance.


